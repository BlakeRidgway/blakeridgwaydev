# blakeridgwaydev

## Design & Build

*For the web server I used*  
**Rust v. 1.55 Nightly**  
**Rocket v 0.4.7**  

*For the website I used*  
**HTML5**  
**CSS3**  
**JavaScript** 

## Pages  

- [x] Home page  
- [x] Projects page  
- [x] About Me page  
- [x] Live Broadcast page
- [x] Footer w/ links (GitLab, LinkedIn, E-Mail, Instagram, Twitter)  

## Hosting  

Website will be hosted on [Linode](https://www.linode.com)  

## Notes

*Maybe add pages from notebook with website design layout?*  

## Hackathon Weekend  

This was created over the weekend of July 23rd 2021 to July 26 2021. With updates coming regularly.  